package es.vqs.first;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import es.vqs.first.entity.Body;
import es.vqs.first.entity.CommonNode;
import es.vqs.first.entity.Div;
import es.vqs.first.entity.Head;
import es.vqs.first.entity.Html;

public class PruebasDOMObjeto {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setIgnoringComments(true);
		factory.setIgnoringElementContentWhitespace(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		File fichero = new File(
				"C:\\Users\\VQuiles\\Downloads\\Materiales Ejercicios XML\\Materiales Ejercicios\\Ejercicio XML.xml");
		Document doc = builder.parse(fichero);
		Node raiz = doc.getFirstChild();
		CommonNode nodoFinal = PruebasDOMObjeto.leerNodo(raiz, null);
		System.out.println(nodoFinal);
	}

	public static CommonNode leerNodo(Node nodo, CommonNode padre) {
		CommonNode nuevoNodo = null;
		Node hijo;
		NodeList raiz = nodo.getChildNodes();

		nuevoNodo = PruebasDOMObjeto.crearNodoObjeto(nodo.getNodeName(), PruebasDOMObjeto.numHijosElemento(raiz));
		PruebasDOMObjeto.vincularNodoPadreHijo(padre, nuevoNodo);
		PruebasDOMObjeto.leerAtributos(nodo.getAttributes(), nuevoNodo);

		for (int i = 0; i < raiz.getLength(); i++) {
			hijo = raiz.item(i);
			if (hijo.getNodeType() == 3) {
				nuevoNodo.setValue(hijo.getNodeValue());
			} else {
				PruebasDOMObjeto.leerNodo(hijo, nuevoNodo);
			}
		}
		return nuevoNodo;
	}

	private static void leerAtributos(NamedNodeMap atributos, CommonNode nodo) {
		Node hijo;
		String atributo = null;
		for (int i = 0; atributos != null && i < atributos.getLength(); i++) {
			hijo = atributos.item(i);
			atributo = hijo.getNodeName() + "=" + hijo.getNodeValue() + " ";
		}
		if (atributo != null && atributo.length() > 1) {
			atributo = atributo.substring(0, atributo.length() - 1);
		}
		nodo.setAttributes(atributo);
	}

	private static int numHijosElemento(NodeList raiz) {
		Node hijo;
		int numHijos = 0;
		for (int i = 0; i < raiz.getLength(); i++) {
			hijo = raiz.item(i);
			if (hijo.getNodeType() == 1) {
				numHijos++;
			}
		}
		return numHijos;
	}

	private static void vincularNodoPadreHijo(CommonNode padre, CommonNode nuevoNodo) {
		if (padre != null) {
			if (padre instanceof Html) {
				if (nuevoNodo instanceof Body) {
					((Html) padre).setBody((Body) nuevoNodo);
				} else if (nuevoNodo instanceof Head) {
					((Html) padre).setHead((Head) nuevoNodo);
				}
			} else if (padre instanceof Head && nuevoNodo instanceof Div) {
				((Head) padre).addDiv((Div) nuevoNodo);
			} else if (padre instanceof Body && nuevoNodo instanceof Div) {
				((Body) padre).addDiv((Div) nuevoNodo);
			}
		}
	}

	private static CommonNode crearNodoObjeto(String nodeName, int numHijos) {
		CommonNode salida = null;
		switch (nodeName) {
		case "html":
			salida = new Html();
			break;
		case "div":
			salida = new Div();
			break;
		case "body":
			salida = new Body(numHijos);
			break;
		case "head":
			salida = new Head(numHijos);
			break;
		}
		return salida;
	}

}
