package es.vqs.first.entity;

public class Html extends CommonNode {
	private Head head;
	private Body body;

	public Head getHead() {
		return this.head;
	}

	public void setHead(Head _head) {
		this.head = _head;
	}

	public Body getBody() {
		return this.body;
	}

	public void setBody(Body _body) {
		this.body = _body;
	}

}
