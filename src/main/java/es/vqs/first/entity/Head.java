package es.vqs.first.entity;

public class Head extends CommonNode {
	private Div[] divs;
	private int contador;

	public Head(int numHijos) {
		this.divs = new Div[numHijos];
		this.contador = 0;
	}

	public Div[] getDivs() {
		return divs;
	}

	public void setDivs(Div[] divs) {
		this.divs = divs;
	}

	public void addDiv(Div nuevoNodo) {
		if (this.contador < this.divs.length) {
			this.divs[this.contador] = nuevoNodo;
			this.contador++;
		}
	}
}
