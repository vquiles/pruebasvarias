package es.vqs.first.entity.jaxb;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlMixed;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Body implements Serializable {

	private static final long serialVersionUID = 1208387430614040278L;

	private Div[] divs;
	private String[] value;

	public Body() {
	}

	@XmlElement(name = "div")
	public Div[] getDivs() {
		return this.divs;
	}

	public void setDivs(Div[] _divs) {
		this.divs = _divs;
	}

	@XmlMixed
	public String[] getValue() {
		return this.value;
	}

	public void setValue(String[] _value) {
		this.value = _value;
	}

}
