package es.vqs.first.entity.jaxb;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Html implements Serializable {

	private static final long serialVersionUID = 8973727111576792114L;
	private Head head;
	private Body body;

	@XmlElement
	public Head getHead() {
		return this.head;
	}

	public void setHead(Head _head) {
		this.head = _head;
	}

	@XmlElement
	public Body getBody() {
		return this.body;
	}

	public void setBody(Body _body) {
		this.body = _body;
	}
}
