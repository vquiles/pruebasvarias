package es.vqs.first.entity;

public abstract class CommonNode {
	private String value;
	private String attributes = "";

	public String getValue() {
		return this.value;
	}

	public void setValue(String _value) {
		this.value = _value;
	}

	public String getAttributes() {
		return this.attributes;
	}

	public void setAttributes(String _attributes) {
		this.attributes = _attributes;
	}
}
