package es.vqs.first;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ManejadorSAX extends DefaultHandler {

	private int tabulaciones = 0;

	@Override
	public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
		this.tabular();
		this.tabulaciones++;
		System.out.print("<" + qName);
		for (int i = 0; i < atts.getLength(); i++) {
			System.out.print(" " + atts.getQName(i) + "=\"" + atts.getValue(i) + "\"");
		}
		System.out.print(">");
		System.out.println();
	}

	@Override
	public void endElement(String uri, String localName, String qName) {
		this.tabulaciones--;
		this.tabular();
		System.out.println("</" + qName + ">");
	}

	@Override
	public void characters(char ch[], int start, int length) {
		this.tabular();
		String str = String.valueOf(ch);
		str = str.substring(start, length + start);
		System.out.println(str);
	}

	private void tabular() {
		for (int i = 0; i < this.tabulaciones; i++) {
			System.out.print("\t");
		}
	}
}
